/**
 * @file   example_action.h
 * @Author Me (me@example.com)
 * @date   September, 2008
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 */

#include "rrg_nf/serial_posix.h"

void SerialComm::SERIAL_EXEPTION(std::string exp)
{
  std::cerr << "[Serial Exeption <port: " << port_name_ << ">] " << exp << std::endl;
}

void SerialComm::SERIAL_REOPEN(std::string exp)
{
  SERIAL_EXEPTION(exp);
  reopenPort();
}

void SerialComm::SERIAL_ABORT(std::string exp)
{
  SERIAL_EXEPTION(exp);
  std::cerr << "[Aborting...]" << exp << std::endl;
  close(fd_);
  exit(-1);
}

SerialComm::SerialComm(std::string _port_name, int _baud_rate) :
    connected_(false), fd_(-1), port_name_(_port_name), baud_rate_(_baud_rate), num_bytes_read_(0), num_bytes_write_(0), ERR_timeout(
        0), ERR_readfailed(0), ERR_tryread(0), ERR_confsend(0), ERR_reopen(0), ERR_crc(0)
{
  read_buff_ = new uint8_t[RX_BUFF_MAX_LEN];
  write_buff_ = new uint8_t[TX_BUFF_MAX_LEN];
}

SerialComm::~SerialComm()
{
  if (isOpen())
    closePort();
}

void SerialComm::openPort()
{

  // Open the port
  //MW fd = open(port.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
  fd_ = open(port_name_.c_str(), O_RDWR | O_NONBLOCK | O_NOCTTY | O_NDELAY, S_IRUSR | S_IWUSR); //O_RDWR | O_SYNC | O_NONBLOCK | O_NOCTTY, S_IRUSR | S_IWUSR);
  if (fd_ < 0)
  {
    std::string extra_msg = "";
    switch (errno)
    {
      case EACCES:
        extra_msg = "You probably don't have premission to open the port for reading and writing.";
        break;
      case ENOENT:
        extra_msg = "The requested port does not exist. Is the robot connected?";
        break;
    }
    SERIAL_ABORT("Unable to open serial port: " + extra_msg);
  }

  tcgetattr(fd_, &oldtio_);

  // Lock the port
  struct flock fl;
  fl.l_type = F_WRLCK;
  fl.l_whence = SEEK_SET;
  fl.l_start = 0;
  fl.l_len = 0;
  fl.l_pid = getpid();

  if (fcntl(fd_, F_SETLK, &fl) != 0)
  {
    SERIAL_ABORT(
        "Device is already locked. Try 'lsof | grep " + port_name_
            + "' to find other processes that currently have the port open.");
  }

  // Change port settings
  struct termios term;
  if (tcgetattr(fd_, &term) < 0)
  {
    SERIAL_ABORT(
        "Unable to get serial port attributes. The port you specified (" + port_name_ + ") may not be a serial port.");
  }

  cfmakeraw(&term);
  cfsetispeed(&term, B57600);
  cfsetospeed(&term, B57600);
  term.c_cflag |= CSTOPB; //myyyyyyyyyyy

  if (tcsetattr(fd_, TCSAFLUSH, &term) < 0)
  {
    SERIAL_ABORT(
        "Unable to get serial port attributes. The port you specified (" + port_name_ + ") may not be a serial port.");
  }

  // Make sure queues are empty before we begin
  if (tcflush(fd_, TCIOFLUSH) != 0)
  {
    SERIAL_ABORT("Tcflush failed. Please report this error if you see it.");
  }

}

void SerialComm::closePort()
{
  if (isOpen())
  {
    if (tcflush(fd_, TCIOFLUSH) != 0)
    {
      SERIAL_ABORT("Tcflush failed. Please report this error if you see it.");
    }

    tcsetattr(fd_, TCSANOW, &oldtio_);

    if (close(fd_) != 0)
    {
      SERIAL_ABORT("Unable to close serial port.");
    }
    fd_ = -1;
  }
}

///reopen port
void SerialComm::reopenPort()
{
  ERR_reopen++;
  if (ERR_reopen > MAX_ERR_reopen)
    SERIAL_ABORT("Too many problems with connection.");

  SERIAL_EXEPTION("Trying to reopen port.");

  if (isOpen())
    closePort();

  sleep(1);
  openPort();
}

bool SerialComm::connect(std::string _port_name, int _baud_rate)
{
  port_name_ = _port_name;
  baud_rate_ = _baud_rate;
  openPort();

#ifdef MULTI_THREAD_RX
  int err = pthread_create(&recv_thread_, NULL, &SerialComm::recvCallback, this);
  if (err)
  {
    printf("thread creation error: %d\n", err);
    return -1;
  }
#endif //MULTI_THREAD_RX
  return isOpen();
}

bool SerialComm::disconnect()
{
  if (isOpen())
    closePort();
  return true;
}

bool SerialComm::isOpen()
{
  return (fd_ != (-1));
}

#ifdef MULTI_THREAD_RX
SerialComm::void_ptr_t SerialComm::recvCallback(void * arg)
{
  //could be without usleep when port is in blocking state, but then could be a problem with blocking port... how it is working? maybe we should copy fd and use read as full duplex port (maybe it is not blocking port when it is read)
  SerialComm* sc = static_cast<SerialComm*>(arg);
  uint8_t tmp;
  sc->connected_ = true;
  while (sc->connected_)
  {
    if (read(sc->fd_, &tmp, 1))
    //block rx_vec_ when want to add new data!
    rx_vec_.push(tmp);
    //usleep(1000);
  }
  return NULL;
}
#endif //MULTI_THREAD_RX
///write data to robot
bool SerialComm::serialWrite(uint8_t* buff, uint32_t len)
{
  int bytes;
#ifdef DEBUGTEXT_OUT
  std::cout << "writing to robot -size: " << nbtw << " data: " << toRobot;
  //for (uint i = 0; i < nbtw; i++)std::cout << (int) write_buff[i] << " ";
  //std::cout << std::endl;
#endif

  // Write the data to the port
  bytes = write(fd_, buff, len);
  if (bytes < 0)
  {
    SERIAL_EXEPTION("error writing to robot.");
    return false;
  }

  if (bytes != len)
  {
    SERIAL_EXEPTION("whole message not written to robot."); //TODO
    return false;
  }

  // Make sure the queue is drained
  // Synchronous IO doesnt always work
  if (tcdrain(fd_) != 0)
  {
    SERIAL_EXEPTION("tcdrain failed.");
    return false;
  }
  return true;
}

int SerialComm::serialRead(uint8_t* buff, uint32_t len)
{
#ifdef  MULTI_THREAD_RX
  if (rx_vec_.size() >= len)
  {
    for (int i = 0; i < len; i++)
    {
      buff[i] = rx_vec_.front();
      rx_vec_.pop();
    }
    return len;
  }
  else
  return -1;
#else //MULTI_THREAD_RX
  int retval = 1, timeout = SERIAL_TIMEOUT_MS;

  struct pollfd ufd[1];
  ufd[0].fd = fd_;
  ufd[0].events = POLLIN;

  if (timeout == 0)
    timeout = -1; // For compatibility with former behavior, 0 means no timeout. For poll, negative means no timeout.

  if ((retval = poll(ufd, 1, timeout)) < 0)
    SERIAL_EXEPTION("pool failed.");

  if (retval == 0)
  {
    SERIAL_EXEPTION("timeout reached.");
    ERR_timeout++;
    if (ERR_timeout > MAX_ERR_timeout)
      SERIAL_REOPEN("Too long waiting for robot.");
  }
  else
    ERR_timeout = 0;

  int ret = read(fd_, (uint8_t *)buff, len);

//  while (ret < len)
//    ret += read(fd_, ((char*)buff) + ret, len - ret);
  if (ret < 0)
  {
    SERIAL_EXEPTION("read failed " + std::string(strerror(errno)));
    ERR_readfailed++;
    if (ERR_readfailed > MAX_ERR_readfailed)
      SERIAL_REOPEN("Too many fails in communication.");
  }
  else
    ERR_readfailed = 0;

  return ret;

#endif //MULTI_THREAD_RX
}

/*
 int SerialComm::waitForFrame(const uint8_t* preamble, uint32_t preamble_len, uint32_t frame_len, uint8_t* buff)
 {

 uint8_t tmp;
 uint32_t i = 0;
 if (preamble_len > 0)
 {
 while (serialRead(&tmp, 1) == 1)
 {
 if (tmp == preamble[i])
 {
 buff[i] = tmp;
 i++;
 if (preamble_len <= i)
 break;
 }
 else
 i = 0;
 }
 }

 while (serialRead(&buff[i], frame_len - (i + 1)) != frame_len - (i + 1))
 usleep(1000);

 return frame_len;
 }
 */

/*mutex use example

 pthread_mutex_t mutex_list;

 void CClient::Lock_sl()
 {
 pthread_mutex_lock(&mutex_list);
 }

 void CClient::Unlock_sl()
 {
 pthread_mutex_unlock(&mutex_list);
 }
 */


/*
int main(int alen, char** avec)
{
  SerialComm serial("/dev/ttyUSB0", 57600);
  serial.connect("/dev/ttyUSB0", 57600);
  uint8_t a[10] = "blabla...";
  serial.serialWrite(a,10);
  while(true)
  {
    //serial.serialWrite(a,10);
  int i = serial.serialRead(a,10);
  serial.serialWrite(a, i);
  usleep(500000);
  }
  return 0;
}
*/
/*

 ///read data from robot

 bool SerialComm::talkRobot()
 {
 int err = 1;

 do
 {
 //toRobot.SetShutDown(true);
 //NOW toRobot.SetSendStatus(true);
 //NOW toRobot.SetFullCommand(true);

 // int nbtw = toRobot.TranslateToByteTab(write_buff);
 ERR_tryread = 0;

 do
 {
 ERR_tryread++;
 if (ERR_tryread > MAX_ERR_tryread)
 {
 SERIAL_EXEPTION("write/read from robot problem.");
 return false;
 }

 //NOW memset(read_buff, 0, fromRobot.fsize);

 //flush();

 if (!writeToRobot())
 {
 //SERIAL_EXEPTION("write to robot problem.");
 continue;
 }
 #ifdef SOFT_DEBUG
 std::cerr << "write+++" << std::endl;
 #endif
 //usleep(1000); //without CRC fail only in start frame

 if (readWithTimeout(fd_, &read_buff_[0], 1, SERIAL_TIMEOUT_MS) != 1)
 {
 ;
 }
 #ifdef SOFT_DEBUG
 std::cerr << "read+++" << std::endl;
 #endif

 #ifdef DEBUGTEXT_IN
 std::cerr << "Schould be '#', it is: " << (uint16_t) read_buff[0] << std::endl;
 #endif
 } while (read_buff_[0] != uint8_t(0x23));

 #ifdef DEBUGTEXT_IN
 std::cerr << "23!" << std::endl;
 #endif

 //NOW if (read_with_timeout(fd, &read_buff[1], fromRobot.fsize - 1, SERIAL_TIMEOUT_MS) != (fromRobot.fsize - 1))
 SERIAL_EXEPTION("partial read from robot.");

 #ifdef DEBUGTEXT_IN
 std::cerr << std::dec << "data: " << "\t";
 for (int j = 0; j < fromRobot.fsize; j++)std::cerr << std::hex << (unsigned int) read_buff[j] << " "; //short
 std::cerr << "\n";
 #endif


 int nbtw = toRobot.TranslateToByteTab(write_buff);
 flush();
 int bytes = write(fd, write_buff, nbtw);
 uint ret=0;
 while (ret < fromRobot.fsize)
 ret += read(fd, ((char*) &read_buff[0]) + ret, fromRobot.fsize - ret);

 //NOW err = fromRobot.TranslateFromByteTab(read_buff);
 if (err == 0)
 {
 SERIAL_ABORT("sth wrong with frame from robot translation.");
 }
 else
 {
 if (err == -1)
 {
 SERIAL_EXEPTION("CRC failed.");
 ERR_crc++;
 if (ERR_crc > MAX_ERR_crc)
 SERIAL_ABORT("serious problem with crc.");
 }
 else
 ERR_crc = 0;
 }
 } while (err != 1);
 return true;
 }

 ///send conf frame only - without answer from robot
 ///remove it!!!!!!!!!!!!!!!

 bool SerialComm::sendConf(bool answer)
 {
 answer = true; //block parameter

 ERR_confsend = 1;
 while (ERR_confsend != 0)
 {
 ERR_confsend++;
 if (ERR_confsend > MAX_ERR_confsend)
 SERIAL_REOPEN("Unable to send configuration to robot.");

 //NOW toRobot.SetSendStatus(answer);
 //NOW toRobot.SetFullCommand(true);

 int16_t _startvel[2] = {0, 0};
 //NOW toRobot.SetMode(RoMegFTR::SPEED);
 //NOW toRobot.SetSpeed(_startvel);

 if (answer)
 {
 if (!talkRobot())
 continue;
 }
 else
 {
 if (!writeToRobot())
 {
 continue;
 }
 //NOW toRobot.SetSendStatus(true);
 if (!writeToRobot())
 {
 continue;
 }
 }
 ERR_confsend = 0;
 }

 return true;
 }
 */

