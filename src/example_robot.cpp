/* 
 * File:   example_robot.cpp
 * Author: mateusz - RRG@WUT rrg.mchtr.pw.edu.pl
 * Author: pweclewski - RRG@WUT rrg.mchtr.pw.edu.pl
 * 
 * This is a example_robot class - an example for nfv2 communication
 * Please modify this example_robot class for your hardware
 * 
 * Created on December 6, 2012, 7:39 PM
 */

#include "rrg_nf/example_robot.h"

example_robot::example_robot(ros::NodeHandle nh, tf::TransformListener& tf) :
n(nh), _tf(tf), rxCnt(0), txCnt(0)
{
    //read parameters if defined in launch files...
    ros::NodeHandle n_private("~");
    //n_private.param<bool> ("map_compare", map_compare, true);

    NFv2_Config2(&NFComBuf, NF_PCAddress, NF_MainModuleAddress);
    crcInit();

    portName = "/dev/ttyACM0";
    //portName = "/dev/ttyUSB0";

    CommPort = new SerialComm();


    CommPort->connect(portName, 57600);

    if (!(CommPort->isOpen()))
    {
        std::cerr << "Connection failed to " << portName << std::endl;
    }
    else
    {
        std::cout << "Connected to " << portName << std::endl;
    }


    robot = new Robot(); // creates specific robot object

    subCmdVel = n.subscribe("cmd_vel", 1, &example_robot::callbackCmdVel, this);

}

example_robot::~example_robot()
{
    setMotorsSpeeds(0, 0);
}

void example_robot::readFromRobot()
{

    int global_counter = 0;
    int n = 0;
    while ((n = CommPort->serialRead(&rxBuf[this->rxCnt], 1)) == 1)
    {
        global_counter++; // temporary timeout counter.. (better to do this in time.now)
        if (global_counter > 2000)
        {
            std::cerr << "\n!!! timeout " << global_counter << std::endl;
            return;
        }
        //        std::cout << "after read ";
        //
        //        std::cout << "\n Attempt with: " << n << " result \t";

        if (n < 0)
        {
            perror("read");
            exit(EXIT_FAILURE);
        }
        else if (n > 0)
        {
            //            for (int i = 0; i < this->rxCnt; i++)
            //            {
            //                std::cout << std::hex << (uint) rxBuf[i] << " ";
            //            }
            //            std::cout << std::dec << "\n";
        }

        // std::cout << "\n no:" << global_counter << " Bytes IN: " << n << " \t";

        uint8_t commArr[16], commCnt, bytesReceived;

        if ((bytesReceived = NF_Interpreter(&NFComBuf, rxBuf, &(this->rxCnt), commArr, &commCnt)) > 0)
        {
            std::cout << std::dec << "NF_Interpreter received " << (int) bytesReceived << " bytes\n";
            std::cout << std::dec << "Frame from robot: ";

            break;
            //sleep(5);
        }
        if (this->rxCnt == 255)
        {
            this->rxCnt = 0;
            break;
        }

    }
}

void example_robot::sendToRobot()
{
    // If communication with requested
    if (commandCnt > 0)
    {
        std::cout << "************************* 1 cycle **********************************************\n";
        txCnt = NF_MakeCommandFrame(&NFComBuf, txBuf, (const uint8_t*) commandArray, commandCnt, NF_MainModuleAddress);
        // Clear communication request
        commandCnt = 0;
        std::cout << "Frame to robot (" << (uint16_t) txCnt << " bytes): ";
        for (int i = 0; i < txCnt; i++)
        {
            std::cout << std::hex << (uint) txBuf[i] << " ";
        }
        std::cout << std::dec << "\n";

        CommPort->serialWrite(txBuf, txCnt);

    }

}

// Robot high level functions -------------------------------------------------

void example_robot::robotUpdate() // Synchronism of communication
{

    // setMotorsSpeeds(robot->robot_speed_left, robot->robot_speed_right);


    //    for (int i = 0; i < 16; i++)
    //    {
    //        uint16_t a = pow(2, i);
    //        robotSetDO(a >> 8 & 0xff, a & 0xff);
    //        usleep(10000);
    //
    //    }
    robotGetStatus();
    
    robotSetDO(0xff, 0xff, 0xff, 0xff, 0xff, 0xff);
    robotGetAI();
    robotReadDistances();


}

void example_robot::robotInit() // Initalization of robot
{

}

void example_robot::robotPanic() // Emergency stop function
{
    setMotorsSpeeds(0, 0);
}

// CALLBACKs  from ROS ------------------------------------------------------

void example_robot::callbackCmdVel(const geometry_msgs::TwistConstPtr &msg)
{
    
    robot->robot_speed_left = 10 * msg->linear.x + 20 * msg->angular.z;
    robot->robot_speed_right = 10 * msg->linear.x - 20 * msg->angular.z;
}

// Robot specific functions ---------------------------------------------------

void example_robot::robotGetVitals()
{

    commandArray[commandCnt++] = NF_COMMAND_ReadDeviceVitals;

    sendToRobot();
    readFromRobot();
}

void example_robot::robotGetStatus()
{

    commandArray[commandCnt++] = NF_COMMAND_ReadDeviceStatus;

    sendToRobot();
    readFromRobot();

    std::cout << "Status:\t";
    for (int i = 0; i < NF_BUFSZ_ReadDeviceStatus; i++)
        std::cout << std::dec << NFComBuf.ReadDeviceStatus.data[i] << " ";
    std::cout << "\n";
}

void example_robot::robotSetDO(int8_t SetDO, int8_t SetD1, int8_t SetD2, int8_t SetD3, int8_t SetD4, int8_t SetD5)
{
    commandArray[commandCnt++] = NF_COMMAND_SetDigitalOutputs;
    NFComBuf.SetDigitalOutputs.data[0] = SetDO;
    NFComBuf.SetDigitalOutputs.data[1] = SetD1;
    NFComBuf.SetDigitalOutputs.data[2] = SetD2;
    NFComBuf.SetDigitalOutputs.data[3] = SetD3;
    NFComBuf.SetDigitalOutputs.data[4] = SetD4;
    NFComBuf.SetDigitalOutputs.data[5] = SetD5;
    sendToRobot();
}

void example_robot::robotSetAO()
{
    //    commandArray[commandCnt++] = NF_COMMAND_SetAnalog....;
    //    ...
    //    sendToRobot();
}

void example_robot::robotGetDI()
{
    commandArray[commandCnt++] = NF_COMMAND_ReadDigitalInputs;

    sendToRobot();
    readFromRobot();
}

void example_robot::robotGetAI()
{

    commandArray[commandCnt++] = NF_COMMAND_ReadAnalogInputs;

    sendToRobot();
    readFromRobot();

    std::cout << "Distance:\t";
    for (int i = 0; i < NF_BUFSZ_ReadAnalogInputs; i++)
        std::cout << std::dec << NFComBuf.ReadAnalogInputs.data[i] << " ";
    std::cout << "\n";

}

void example_robot::robotReadDistances()
{

    commandArray[commandCnt++] = NF_COMMAND_ReadDistance;

    sendToRobot();
    readFromRobot();

    std::cout << "Distance:\t";
    for (int i = 0; i < NF_BUFSZ_ReadDistance; i++)
        std::cout << std::dec << NFComBuf.ReadDistance.data[i] << " ";
    std::cout << "\n";
}

void example_robot::setRobotServos()
{
    //    commandArray[commandCnt++] = NF_COMMAND_SetServosPosition;
    //    NFComBuf.SetServosPosition.data[0] = a1;
    //    NFComBuf.SetServosPosition.data[1] = a1;
    //    NFComBuf.SetServosPosition.data[2] = a1;
    //    NFComBuf.SetServosPosition.data[3] = a1;
    //    NFComBuf.SetServosPosition.data[4] = a1;
    //    NFComBuf.SetServosPosition.data[5] = a1;
    //    NFComBuf.SetServosPosition.data[6] = a1;
    //    NFComBuf.SetServosPosition.data[7] = a1;

    sendToRobot();

}

void example_robot::setMotorsSpeeds(int motor_left, int motor_right)
{

    if (motor_left > robot_left_max)
    {
        motor_left = robot_left_max;
        std::cout << "speed exceeded maximum...\n";
    }
    if (motor_right > robot_right_max)
    {
        motor_right = robot_right_max;
        std::cout << "speed exceeded maximum...\n";
    }

    //    NFComBuf.SetDrivesMode.data[0] = NF_DrivesMode_SPEED;
    //    NFComBuf.SetDrivesMode.data[1] = NF_DrivesMode_SPEED;
    //    commandArray[commandCnt++] = NF_COMMAND_SetDrivesMode;
    //
    //    NFComBuf.SetDrivesSpeed.data[0] = motor_left;
    //    NFComBuf.SetDrivesSpeed.data[1] = motor_right;
    //    commandArray[commandCnt++] = NF_COMMAND_SetDrivesSpeed;

    sendToRobot();

}
