/**
 * @file   example_action.h
 * @Author Me (me@example.com)
 * @date   September, 2008
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 */

#ifndef SERIALCOMM_H
#define	SERIALCOMM_H

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <termios.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdlib.h>

#include <iostream>

#include <sys/time.h>
#include "poll.h"

//#define MULTI_THREAD_RX
#ifdef MULTI_THREAD_RX

#include<queue>
#include<vector>

#endif

///default complete port name
#define DEFAULT_PORT "/dev/RoMegAtMchtr"
///default baudrate
#define DEFAULT_BAUD 57600//921600
///serial sth delay [ms]
#define SERIAL_TIMEOUT_MS 0

#define RX_BUFF_MAX_LEN 255
#define TX_BUFF_MAX_LEN 255

/**
 Communication with ROMEG/Courier robot
 protocol: RS-485
 baudrate: 57600bps//921600bps
 parity bit: off,
 data bits: 8
 stop bits: 2
 */

/** 
 * @class ClassTemplate
 *
 * @brief This is an example class.
 *
 * This comment block is @e required for all class declarations.
 * Please remove comments that are bracketed by [..]. These comments are there
 * to provide instructions to developers while writing their code.
 * Obvious member variables and functions, such as get and set routines and
 * default constructors and destructors, should not be documented as this
 * clutters the files. Use standard C++ comments for those comments you wish
 * Doxygen to ignore. If the class has many members - you may consider
 * providing separate public, protected, private sections for member functions
 * and member variables to improve readability. In addition it may be useful to
 * form member groups preceded by a header as shown below.
 *
 * Please note that the \$Header\$ keyword specified below is a RCS keyword,
 * and will inflate into the version, name, etc for this file.
 *
 * @author Some Body
 *
 * $Header $
 */
class SerialComm
{
public:

  /**
   * @name    Example API Actions
   * @brief   Example actions available.
   * @ingroup example
   *
   * Here is an example of inserting a mathematical formula into the text:
   * The distance is computed as /f$\sqrt{ (x_2-x_1)^2 + (y_2 - y_1)^2 }/f$
   * If we wanted to insert the formula centered on a separate line:
   * /f[
   * \sqrt{ (x_2-x_1)^2 + (y_2 - y_1)^2 }
   * /f]
   * Please note that all formulas must be valid LaTeX math-mode commands.
   * Additionally, to be processed by Doxygen, the machine used must have
   * LaTeX installed. Please see the Doxygen manual for more information
   * about installing LaTeX locally.
   *
   * @param [in] repeat  Number of times to do nothing.
   *
   * @retval TRUE   Successfully did nothing.
   * @retval FALSE  Oops, did something.
   *
   * Example Usage:
   * @code
   *    example_nada(3); // Do nothing 3 times.
   * @endcode
   */
  /**constructor
   * @param port_name name of serial port which robot is conected (full name e.g. "/dev/ttyS0","/dev/ttyUSB0", etc.)
   * @param rate baudrate for serial connection
   */
  SerialComm(std::string _port_name = DEFAULT_PORT, int _baud_rate = DEFAULT_BAUD);

  ///destructor
  virtual ~SerialComm();

  ///connect
  bool connect(std::string _port_name = DEFAULT_PORT, int _baud_rate = DEFAULT_BAUD);

  bool disconnect();

  ///true when port is open
  bool isOpen();
  bool connected_;

  ///write data to serial
  bool serialWrite(uint8_t* buff, uint32_t len);
  ///read data from serial
  int serialRead(uint8_t* buff, uint32_t len);

  //int waitForFrame(const uint8_t* preamble, uint32_t preamble_len, uint32_t frame_len, uint8_t* buff);

  ///read data from robot
  //bool talkRobot();
  ///send conf frame only - without answer from robot
  //bool SendConf(std::string, int val);
  //bool sendConf(bool answer = false);

private:

#ifdef MULTI_THREAD_RX
  typedef void* void_ptr_t;
  pthread_t recv_thread_;
  static void_ptr_t recvCallback(void * arg);
#endif

  void SERIAL_EXEPTION(std::string exp);
  void SERIAL_REOPEN(std::string exp);
  void SERIAL_ABORT(std::string exp);

  //port identifier
  int fd_;

  ///serial port name
  std::string port_name_;
  ///baudrate of transmission
  int baud_rate_;
  ///number of bytes to read
  int num_bytes_read_;
  ///number of bytes to write
  int num_bytes_write_;
  ///serial port read buffer
  uint8_t* read_buff_;
  ///serial port write buffer
  uint8_t* write_buff_;
  ///store serial port settings
  struct termios oldtio_;

#ifdef MULTI_THREAD_RX
  std::queue< std::vector<uint8_t> > rx_vec_;
#endif

  ///open port
  void openPort();
  ///close port
  void closePort();
  ///reopen port
  void reopenPort();

  ///communication timeout errors counter
  int ERR_timeout;
  ///number of accepted communication timeout errors
  static const int MAX_ERR_timeout = 10;
  ///read errors counter
  int ERR_readfailed;
  ///number of accepted read errors
  static const int MAX_ERR_readfailed = 10;
  ///read repeats counter
  int ERR_tryread;
  ///number of accepted read repeats
  static const int MAX_ERR_tryread = 10;
  ///sent configuration attempts counter
  int ERR_confsend;
  ///number of accepted sent configuration attempts
  static const int MAX_ERR_confsend = 10;
  ///reopen attempts counter
  int ERR_reopen;
  ///number of accepted reopen attempts
  static const int MAX_ERR_reopen = 10;
  ///crc errors counter
  int ERR_crc;
  ///number of accepted crc errors
  static const int MAX_ERR_crc = 10;
};

#endif	/* SERIALCOMM_H */

